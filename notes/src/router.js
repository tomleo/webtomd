import Vue from 'vue'
import Router from 'vue-router'
import AddUrl from './views/AddUrl.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
        path: '/',
        name: 'add-url',
        component: AddUrl,
    },
  ]
})
