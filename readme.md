# Webtomd

This utility downloads website articles in markdown for archiving, then creates
a notes file linking to the markdown archive. This utility should make taking
notes on the web better.

To make this callable anywhere

``` bash
ln -s /path/webtomd.py ~/.local/bin/webtomd
sudo chmod +x ~/.local/bin/webtomd
```

# TODO
## Replace instapaper with own GUI
- Look into https://github.com/codelucas/newspaper instead of readability API



