var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var WebDocSchema = new Schema({
    title: String,
    url: String,
    body: String
});

var WebDocSchema = mongoose.model("WebDoc", WebDocSchema);
module.exports WebDocSchema;
