const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const https = require('https')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

// const mongodb_conn_module = require('./mongodbConnModule');
// var db = mongodb_conn_module.connect();
// var Document = require("../models/webdoc");

app.post('/get-url', (req, res) => {
    const TEST_URL = 'https://blog.tomleo.com/post/171797128415/batch-convert-flac-files-to-mp3-files-you-can'
    // req.body.url
    https.get(TEST_URL, (resp) => {
        let data = '';
        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
          data += chunk;
        });
        // The whole response has been received. Print out the result.
        resp.on('end', () => {
          console.log(data);
        })
        return res.send({
            success: true,
            data: data,
        });
    })
    .on("error", (err) => {
      console.log("Error: " + err.message);
    });
});

app.listen(process.env.PORT || 8081)
