#!/usr/bin/python3

import os
import re
import datetime
from urllib.parse import urlparse

import requests


def get_filename_from_url_path(url_path):
    """Convert path 2017/07/10/blog-post to 2017-07-10-blog-post"""
    _fname = "%s" % re.sub('\ |\/', '-', url_path)
    if _fname.startswith('-'):
        _fname = _fname[1:]
    if _fname.endswith('-'):
        _fname = _fname[:-1]
    return _fname

def create_folder(folder_path):
    """Returns True if folder was created, False if already exists"""
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
        return True
    return False

def should_create_file(file_path):
    """
    Return true if file should be created/overwritten
    """
    if os.path.isfile(file_path):
        should_overwrite = input('file already exists, overwrite? (Y,N): ')
        if not should_overwrite in ['Y', 'y', 'yes', 'Yes']:
            return False
    return True

def save_website_to_markdown(url, file_path):
    FYMD = "http://fuckyeahmarkdown.com/go/"
    req = requests.post('http://fuckyeahmarkdown.com/go/', data={
        'u': url
    })
    if req.status_code == 200:
        with open(file_path, 'w+') as fout:
            fout.write(req.text)
        return True
    return False

def save_notes_markdown_file(file_path, archive_path, fname):
    with open(file_path, 'w+') as fout:
        fname = re.sub('-', ' ', fname).capitalize()
        fout.write('# %s\n' % (fname))
        fout.write('[%s](%s)\n\n' % (date_formatted_fname(fname), archive_path))


def date_formatted_fname(fname):
    # TODO: rename this something like get_formated_filename
    today = datetime.datetime.now().strftime('%Y-%m-%d')
    return "%s--%s.md" % (today, fname)


def website_to_markdown(url, notes=True):
    """
    Creates a website backup in markdown and a notes file with a link to the
    markdown archive.

    This works especially well for organizing articles you have read and
    syncing them to dropbox.
    """
    NOTES_DIR = os.path.join(os.path.expanduser('~'), "Dropbox", "reading")
    ARCHIVE_DIR = os.path.join(os.path.expanduser('~'), "Dropbox", 'reading_archive')


    url_obj = urlparse(url)
    folder_name = url_obj.netloc
    _fname = get_filename_from_url_path(url_obj.path)

    # Archvie website content to markdown file
    # fname = "%s--%s.md" % (today, _fname)
    fname = date_formatted_fname(_fname)
    archive_sub_path = os.path.join(ARCHIVE_DIR, folder_name)

    create_folder(archive_sub_path)
    archive_file_path = os.path.join(archive_sub_path, fname)
    if should_create_file(archive_file_path):
        if save_website_to_markdown(url, archive_file_path):
            print("Success! %s" % (archive_file_path))
        else:
            print("Failed %s" % (archive_file_path))

    # Create website notes markdown file
    if not notes:
        return

    notes_sub_path = os.path.join(NOTES_DIR, folder_name)
    create_folder(notes_sub_path)
    notes_fname = '%s.md' % _fname
    file_path_notes = os.path.join(notes_sub_path, notes_fname)
    if should_create_file(file_path_notes):
        save_notes_markdown_file(file_path_notes, archive_file_path, _fname)
        print("Notes %s Created" % (file_path_notes))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Download a webpage as markdown.')
    parser.add_argument('-u', '--url', type=str,
                        help='website to convert into markdown')
    parser.add_argument('-n', '--notes', action='store_false',
                        help='Skip creating notes file?')
    website_to_markdown(**vars(parser.parse_args()))

